﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:lib="http://library.by/catalog"
  exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <!-- Content:template -->

  <xsl:template match="/lib:catalog">
    <xsl:element name="rss">
      <xsl:attribute name="version">2.0</xsl:attribute>
      <xsl:element name="channel">
        <xsl:element name="title">RSS-лента</xsl:element>
        <xsl:element name="link">http://library.by/catalog</xsl:element>
        <xsl:element name="description">Каталог новых поступлений.</xsl:element>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="/lib:catalog/lib:book">
    <xsl:element name="item">
      <xsl:element name="title">
        <xsl:value-of select="lib:title"/>
      </xsl:element>
      <xsl:element name="author">
        <xsl:value-of select="lib:author"/>
      </xsl:element>
      <xsl:element name="description">
        <xsl:value-of select="lib:description"/>
      </xsl:element>
      <xsl:element name="genre">
        <xsl:value-of select="lib:genre"/>
      </xsl:element>
      <xsl:element name="publicationDate">
        <xsl:value-of select="lib:registration_date"/>
      </xsl:element>
      <xsl:if test="lib:genre = 'Computer' and lib:isbn">
        <xsl:element name="source">
          http://my.safaribooksonline.com/<xsl:value-of select="lib:isbn"/>/
        </xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <xsl:template match="@* | node()"/>

</xsl:stylesheet>
