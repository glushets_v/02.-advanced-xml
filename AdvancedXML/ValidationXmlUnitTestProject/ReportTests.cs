﻿using Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ValidationXmlUnitTestProject
{
    [TestClass]
    public class ReportTests
    {
        [TestMethod]
        public void CreateHtmlReport()
        {
            var libraryWorker = new LibraryWorker();

            libraryWorker.CreateHtmlReport("books.xml", "htmlReportResult.html");
        }
    }
}
