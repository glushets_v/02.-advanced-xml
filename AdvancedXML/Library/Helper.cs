﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Library
{
    public class Helper
    {
        private Dictionary<string, int> _gengersCount;

        public List<string> UniqGenres { get; set; }

        public Helper()
        {
            UniqGenres = new List<string>();
            _gengersCount = new Dictionary<string, int>();
        }

        public XPathNavigator[] GetGenders()
        {
            XPathNavigator[] genders = UniqGenres.Select(s => new XElement("gender", s).CreateNavigator()).ToArray();

            return genders;
        }

        public bool IsUnProcessed(string genre)
        {
            return UniqGenres.All(x => x != genre); // не содержится в массиве, т.е. не обработан
        }

        public void AddProcessedGenre(string genre)
        {
            if (UniqGenres.All(x => x != genre))
            {
                UniqGenres.Add(genre);
            }
        }

        public int GetCountForGenre(string genre)
        {
            int count;
            if (_gengersCount.TryGetValue(genre, out count))
            {
                return count;
            }

            return 0;
        }

        public void UpCount(string genre)
        {
            int count;

            if (_gengersCount.TryGetValue(genre, out count))
            {
                _gengersCount[genre] = ++count;
            }
            else
            {
                _gengersCount[genre] = 1;
            }
        }

        public int GetSumCount()
        {
            int sum = 0;

            foreach (var genre in UniqGenres)
            {
                sum += GetCountForGenre(genre);
            }
            return sum;
        }
    }
}