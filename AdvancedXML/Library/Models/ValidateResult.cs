﻿using System.Collections.Generic;

namespace Library.Models
{
    public class ValidateResult
    {
        public ValidateResult()
        {
            ErrorMessages = new List<MessageInfo>();
        }

        public bool IsValid { get; set; }

        public List<MessageInfo> ErrorMessages { get; set; }
    }
}
