﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:lib="http://library.by/catalog"
                xmlns:helper="http://tempuri.org/helper"
                exclude-result-prefixes="msxsl"
>

  <xsl:output method="html" indent="yes"/>
  <xsl:param name="currentDate" />

  <xsl:param name="genders" select="helper:GetGenders()"/>

  <xsl:template name="genreTable">
    <xsl:param name="genre">Empty</xsl:param>

    <table>
      <tr>
        <th>№</th>
        <th>Автор</th>
        <th>Название</th>
        <th>Дата издания</th>
        <th>Дата регистрации</th>
      </tr>

      <xsl:for-each select="/lib:catalog/lib:book/lib:genre[text()=$genre]">
        <xsl:value-of select="helper:UpCount($genre)"/>
        <tr>
          <td>
            <xsl:value-of select="helper:GetCountForGenre(.)"/>
          </td>
          <td>
            <xsl:value-of select="../lib:author"/>
          </td>
          <td>
            <xsl:value-of select="../lib:title"/>
          </td>
          <td>
            <xsl:value-of select="../lib:publish_date"/>
          </td>
          <td>
            <xsl:value-of select="../lib:registration_date"/>
          </td>
        </tr>

      </xsl:for-each>
    </table>
    <br />
    <span>
      Итого: <xsl:value-of select="helper:GetCountForGenre(lib:genre)"/>
    </span>
    <br />

  </xsl:template>

  <xsl:template match="/lib:catalog">
    <html>
      <body>
        <h2>
          Отчет по книгам за <xsl:value-of select="$currentDate"/>
        </h2>
        <br/>

        <xsl:for-each select="/lib:catalog/lib:book">

          <xsl:if test="helper:IsUnProcessed(lib:genre)">

            <xsl:call-template name="genreTable">
              <xsl:with-param name="genre">
                <xsl:value-of select="lib:genre"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:if>

          <xsl:value-of select="helper:AddProcessedGenre(lib:genre)"/>

        </xsl:for-each>

        <xsl:element name="h2">
          Всего книг в библиотеке: <xsl:value-of select="helper:GetSumCount()"/>
        </xsl:element>

        <table>
          <tr>
            <th>Жанр</th>
            <th>Количество</th>
          </tr>
          <xsl:for-each select="helper:GetGenders()">
            <tr>
              <th>
                <xsl:value-of select="."/>
              </th>
              <th>
                <xsl:value-of select="helper:GetCountForGenre(.)"/>
              </th>
            </tr>
          </xsl:for-each>
        </table>
        
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
