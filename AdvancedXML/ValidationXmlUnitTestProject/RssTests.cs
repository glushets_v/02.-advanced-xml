﻿using Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ValidationXmlUnitTestProject
{
    [TestClass]
    public class RssTests
    {
        [TestMethod]
        public void CreateRss()
        {
            var libraryWorker = new LibraryWorker();

            libraryWorker.CreateRss("ValidBooks.xml", "rssResult.xml");
        }
    }
}
