﻿namespace Library.Models
{
    public class MessageInfo
    {
        public int LineNumber { get; set; }

        public int LinePosition { get; set; }

        public string Message { get; set; }
    }
}