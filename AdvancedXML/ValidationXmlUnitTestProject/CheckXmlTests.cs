﻿using System.Linq;
using Library;
using Library.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ValidationXmlUnitTestProject
{
    [TestClass]
    public class CheckXmlTests
    {

        [TestMethod]
        public void CheckXml_Valid()
        {
            var libraryWorker = new LibraryWorker();

            ValidateResult result = libraryWorker.CheckXml("ValidBooks.xml");

            Assert.IsTrue(result.IsValid);
            Assert.IsFalse(result.ErrorMessages.Any());
        }

        [TestMethod]
        public void CheckXml_Isbn_Value_Invalid()
        {
            var libraryWorker = new LibraryWorker();

            ValidateResult result = libraryWorker.CheckXml("IsbnCheck.xml");

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ErrorMessages.Any());
            Assert.AreEqual(4, result.ErrorMessages.First().LineNumber);
            Assert.AreEqual(17, result.ErrorMessages.First().LinePosition);
        }

        [TestMethod]
        public void CheckXml_Genre_Value_Invalid()
        {
            var libraryWorker = new LibraryWorker();

            ValidateResult result = libraryWorker.CheckXml("GenreInvalid.xml");

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ErrorMessages.Any());
            Assert.AreEqual(7, result.ErrorMessages.First().LineNumber);
            Assert.AreEqual(21, result.ErrorMessages.First().LinePosition);
        }
        
        [TestMethod]
        public void CheckXml_DateInvalid_Value_Invalid()
        {
            var libraryWorker = new LibraryWorker();

            ValidateResult result = libraryWorker.CheckXml("DateInvalid.xml");

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ErrorMessages.Any());
            Assert.AreEqual(9, result.ErrorMessages[0].LineNumber);
            Assert.AreEqual(31, result.ErrorMessages[0].LinePosition);
            Assert.AreEqual(15, result.ErrorMessages[1].LineNumber);
            Assert.AreEqual(36, result.ErrorMessages[1].LinePosition);
        }

        [TestMethod]
        public void CheckXml_BookId_Invalid()
        {
            var libraryWorker = new LibraryWorker();

            ValidateResult result = libraryWorker.CheckXml("InValidBookId.xml");

            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.ErrorMessages.Any());
            Assert.AreEqual(18, result.ErrorMessages.First().LineNumber);
            Assert.AreEqual(4, result.ErrorMessages.First().LinePosition);
        }
    }
}
