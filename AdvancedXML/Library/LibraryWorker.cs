﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;
using Library.Models;

namespace Library
{
    public class LibraryWorker
    {
        public ValidateResult CheckXml(string inputFilePath)
        {
            var result = new ValidateResult
            {
                IsValid = true
            };
            var settings = new XmlReaderSettings();

            settings.Schemas.Add("http://library.by/catalog", "BookXMLSchema.xsd");
            settings.ValidationEventHandler +=
                delegate (object sender, ValidationEventArgs e)
                {
                    result.IsValid = false;
                    result.ErrorMessages.Add(new MessageInfo
                    {
                        LineNumber = e.Exception.LineNumber,
                        LinePosition = e.Exception.LinePosition,
                        Message = e.Message
                    });

                    Console.WriteLine("[{0}:{1}] {2}", e.Exception.LineNumber, e.Exception.LinePosition, e.Message);
                };

            settings.ValidationFlags = settings.ValidationFlags | XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationType = ValidationType.Schema;

            using (var reader = XmlReader.Create(inputFilePath, settings))
            {
                while (reader.Read()) ;
            }
            return result;
        }

        public void CreateRss(string sourceFilePath, string outputPath)
        {
            var xslt = new XslCompiledTransform(enableDebug: true);
            xslt.Load("Rss.xslt");

            XmlDocument doc = new XmlDocument();
            doc.Load(sourceFilePath);

            using (var stream = File.Create(outputPath))
            {
                xslt.Transform(XmlReader.Create(sourceFilePath), null, stream);
            }
        }

        public void CreateHtmlReport(string sourceFilePath, string outputPath)
        {
            var xslt = new XslCompiledTransform(enableDebug: true);
            xslt.Load("HtmlReport.xslt");

            XmlDocument doc = new XmlDocument();
            doc.Load(sourceFilePath);

            XsltArgumentList arguments = new XsltArgumentList();
            arguments.AddParam("currentDate", string.Empty, DateTime.Now.ToString("yyyy-MM-dd"));
            arguments.AddExtensionObject("http://tempuri.org/helper", new Helper());

            using (var stream = File.Create(outputPath))
            {
                xslt.Transform(XmlReader.Create(sourceFilePath), arguments, stream);
            }
        }
    }
}
